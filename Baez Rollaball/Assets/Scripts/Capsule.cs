﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Capsule : MonoBehaviour
{
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    private int count;

    // Start is called before the first frame update
    void Start()
    {
        SetCountText();
		winTextObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SetCountText()
	{
		countText.text = "Count: " + count.ToString();
		if(count >= 12)
		{
			winTextObject.SetActive(true);
		}
	}

    private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("PickUp"))
		{
			other.gameObject.SetActive(false);
			count = count + 1;

			SetCountText();
		}
		
	}
}


